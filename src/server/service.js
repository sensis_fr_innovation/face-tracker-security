var admin = require("firebase-admin")
var serviceAccount = require('./fr-innovation-firebase-adminsdk.json')
// Set the configuration for your app
// TODO: Replace with your project's config object
// var config = {
//   apiKey: "AIzaSyD0hKxcNdtvHiXNmv2terXOy_73UfcZai4",
//   authDomain: "projectId.firebaseapp.com",
//   databaseURL: "https://databaseName.firebaseio.com",
//   storageBucket: "bucket.appspot.com"
// };

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://fr-innovation.firebaseio.com"
});
// admin.initializeApp(config);

// Get a reference to the database service
var database = admin.database()
var ref = database.ref("Users/Adri/Username");


function getData(){
  ref.on("value", function(snapshot) {
    console.log(snapshot.val());
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });
}

module.exports = {
  getData
}